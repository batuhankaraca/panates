import { Component } from '@angular/core';
import { ApiService } from './service/api.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  hotel_information: any = [];
  public hotel = { id: "" };

  constructor(private apiService: ApiService, private http: HttpClient) {

  }

  getValue() {
    this.hotel = { id: this.hotel.id };
    console.log(this.hotel.id);
    this.callServer(this.hotel);
    this.hotelsId();
  }

  callServer(hotel) {
    const headers = new HttpHeaders()
      .set('Authorization', 'my-auth-token')
      .set('Content-Type', 'application/json');
    // ports:
    // :3000 - to call nodejs server
    // :3001 - to call aspnet core server
    this.http.post(`http://localhost:3000/ping`, JSON.stringify(hotel), {
      headers: headers
    })
      .subscribe(data => {
        console.log(data);
      });
  }

  async hotelsName() {
    this.apiService.getHotelsName(this.hotel.id).subscribe((data) => {
      this.hotel_information = data;
      console.log(this.hotel_information);

    });
  }
  async hotelsId() {
    this.apiService.getHotelsId().subscribe((data) => {
      this.hotel_information = data;
      console.log("hello", this.hotel_information);

    });
  }
}
