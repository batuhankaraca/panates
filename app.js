const express = require('express')
const bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
const app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}) );

app.all("/*", function(req, res, next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  next();
});

var url = "mongodb+srv://dbUser:dbPassword@cluster0-6ehlt.mongodb.net/?ssl=true&authSource=admin";

app.post('/ping', function (req, res) {
  req.body.id = `${req.body.id}`;
  console.log("ne",req.body.id);
  
  res.send(req.body)
    connectionDB(req.body.id);
})

function connectionDB(id){
    console.log("connec",id);
    
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("sample_airbnb");
        dbo.collection("listingsAndReviews").findOne({_id: id}, function(err, result) {
          if (err) throw err;
          console.log("result",result._id);
          if (result) {
            console.log("result_2",result._id);
    
            app.get('/id', function (req, res) {
                console.log("son result",result._id);
                           
                var myJSON = JSON.stringify(result);
                res.end(myJSON);
            });
    
        } else {
            console.log(`No listings found`);
        }
        });
      });
}
app.listen(3000, function () {
  console.log('Nodejs app listening on port 3000')
})